package 二叉堆;

import java.util.Random;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 基于数组实现的最大堆（根节点比所有的左右节点都大）
 * @since 2021/1/21 10:50
 **/

public class MaxHeap<E extends Comparable<E>> {
    public Array<E> array;

    public MaxHeap() {
        array = new Array<>();
    }

    public MaxHeap(int capacity) {
        array = new Array<>(capacity);
    }

    public MaxHeap(E[] data){
        array = new Array<E>(data);
    }

    // 返回堆中的元素个数
    public int size() {
        return array.getSize();
    }

    // 返回一个布尔值, 表示堆中是否为空
    public boolean isEmpty() {
        return array.isEmpty();
    }

    // 返回完全二叉树的数组表示中，一个索引所表示的元素的父亲节点的索引
    public int parent(int index) {
        if (index == 0) {
            throw new IllegalArgumentException("index-0 doesn't have parent.");
        }
        return (index - 1) / 2;
    }

    // 返回完全二叉树的数组表示中，一个索引所表示的元素的左孩子节点的索引
    public int leftNode(int index) {
        return 2 * index + 1;
    }

    // 返回完全二叉树的数组表示中，一个索引所表示的元素的右孩子节点的索引
    public int rightNode(int index) {
        return 2 * index + 2;
    }

    //向最大堆中添加元素
    public void add(E e) {
        array.addLast(e);
        sideUp(array.getSize() - 1);
    }

    private void sideUp(int index) {
        while (index > 0 && array.get(index).compareTo(array.get(parent(index))) > 0) {
            array.swap(index, parent(index));
            index = parent(index);
        }
    }

    public E getMax() {
        return array.get(0);
    }

    //从最大堆中去除最大的元素
    public void remove() {
        //最大的元素和最后一个元素交换位置
        array.swap(0, size() - 1);
        //删除最大元素
        array.removeLast();
        //下浮操作
        sideDown(0);
    }

    /**
     * 下浮索引位置为k的元素
     * @param k
     */
    private void sideDown(int k) {
        while (leftNode(k) < size()) {
            int i = leftNode(k);
            if (rightNode(k) < size() && array.get(rightNode(k)).compareTo(array.get(leftNode(k))) > 0) {
                i = rightNode(k);
            }
            if (array.get(k).compareTo(array.get(i)) < 0){
                array.swap(k, i);
                k = i;
            }
        }
    }

    /**
     *    从最大堆中 再放入一个新元素
     *    方式一： 从最大堆中取出元素（remove操作） 然后再执行add操作
     *    方式二： 直接将新添加的元素与最大的元素替换 然后下浮操作
     */
    public void replace(E e){
        array.set(0, e);
        sideDown(0);
    }

    /**
     * 将一个新数组按照堆的形状排列
     */
    public void heapify(){
        //最后一个节点元素父节点的索引位置
        int index = parent(size() - 1);
        while (index >= 0 ){
            sideDown(index);
            index--;
//            if (rightNode(index) > size() - 1){
//                sideDown(index);
//            }
        }
    }

    /**
     *
     * @param data
     * @param isHeapify 是否将一个新数组排列成堆形状
     * @return
     */
    public double testHeapify(E[] data,boolean isHeapify){
        long beginTime = System.currentTimeMillis();
        if (isHeapify){
            heapify();
        }else {
            for (int i = 0; i < data.length; i++) {
                array.add(i, data[i]);
            }
        }
        long endTime = System.currentTimeMillis();
        return endTime - beginTime;
    }
    public static void main(String[] args) {
//        MaxHeap<Integer> maxHeap = new MaxHeap<>(20);
//        Random random = new Random();
//        for (int i = 0; i < 19; i++) {
//            maxHeap.add(random.nextInt(100));
//        }
//        System.out.println(maxHeap.array);
//        System.out.println(maxHeap.getMax());
//        maxHeap.remove();
//        System.out.println(maxHeap.array);
//        maxHeap.remove();
//        System.out.println(maxHeap.array);
//        maxHeap.remove();
//        System.out.println(maxHeap.array);
        System.out.println("====================");
        Integer[] a = {15,17,19,13,22,16,28,30,41,62};
        MaxHeap<Integer> heap = new MaxHeap<>(a);
        System.out.println(heap.array);
        heap.heapify();
        System.out.println(heap.array);
        System.out.println("==================");
        MaxHeap<Integer> maxHeap2 = new MaxHeap<>(20);
        Random random = new Random();
        Integer[] integers = new Integer[1000000];
        for (int i = 0; i < 1000000; i++) {
            integers[i] = random.nextInt(Integer.MAX_VALUE);
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//        System.out.println(Arrays.asList(integers));
        double d1 = maxHeap2.testHeapify(integers, true);
        System.out.println("d1 = " + d1);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");


        MaxHeap<Integer> maxHeap3 = new MaxHeap<>(20);
        Random random2 = new Random();
        Integer[] integers2 = new Integer[1000000];
        for (int i = 0; i < 1000000; i++) {
            integers2[i] = random2.nextInt(Integer.MAX_VALUE);
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//        System.out.println(Arrays.asList(integers));
        double d2 = maxHeap3.testHeapify(integers, false);
        System.out.println("d2 = " + d2);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }
}
