package 二分搜索树;

import java.util.*;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 构建二分搜索树
 * 性质： 每个节点都大于所有左节点的值 小于所有右节点的值
 * @since 2021/1/15 12:35
 **/

public class BST2<E extends Comparable<E>> {
    public Node root;
    public int size;

    public class Node{
        E value;
        Node left,right;

        public Node(E value, Node left, Node right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }
        public Node(){
            this(null,null,null);
        }
        public Node(E value){
            this(value,null,null);
        }
    }

    public BST2(){
        root = new Node();
        size = 0;
    }

    public void add(E e){
        //添加的第一种方式
        /*if (root.value == null){
            root = new Node<>(e);
            size++;
        }else {
            addNode(e,root);
            size++;
        }*/

        //添加的第二种方式
        root = addNode2(e, root);
    }

    /**
     * 递归的第一种方式
     * @param e
     * @param node
     * @return
     */
    public Node addNode(E e,Node node){

        if (node == null){
            node = new Node(e);
            return node;
        }
        if (e.compareTo(node.value) > 0 && node.right !=null){
            addNode(e,node.right);
        }
        if (e.compareTo(node.value) < 0 && node.left !=null){
            addNode(e,node.left);
        }
        if (e.compareTo(node.value) > 0){
            Node addNode = addNode(e, node.right);
            node.right = addNode;
        }
        if (e.compareTo(node.value) < 0){
            Node addNode = addNode(e,node.left);
            node.left = addNode;
        }
        return node;
    }

    /**
     * 有问题，待测
     * @param e
     * @param node
     * @return
     */
    public Node addNode2(E e,Node node){
        if (node == null || node.value == null){
            node = new Node(e);
            size++;
        }

        if (e.compareTo(node.value) < 0){
            node.left = addNode2(e, node.left);
        }
        if (e.compareTo(node.value) > 0){
            node.right = addNode2(e,node.right);
        }
        return node;
    }

    /** 采用非递归实现前序遍历  **/
    public void preOrderNR(){
        //通过使用栈的方式实现  后进先出
        Stack<Node> stack = new Stack<>();
        stack.push(root);

        while (!stack.isEmpty()){
            Node pop = stack.pop();
            if (pop.value != null){
                System.out.println(pop.value);
            }

            if (pop.right != null){
                stack.push(pop.right);
            }
            if (pop.left != null){
                stack.push(pop.left);
            }
        }
    }

    /**
     * 层序遍历 又叫广度优先遍历
     */
    public void levelOrder(Node node){
        Queue<Node> queue = new LinkedList<>();
        queue.add(node);

        while (!queue.isEmpty()){
            Node poll = queue.poll();
            System.out.println(poll.value);
            if (poll.left != null){
                queue.add(poll.left);
            }
            if (poll.right != null){
                queue.add(poll.right);
            }
        }
    }

    /**前序遍历 遍历顺序： 根，左，右**/
    public void preOrder(Node root){
        if (root == null){
            return;
        }
        System.out.println(root.value);
        if (root.left != null){
            preOrder(root.left);
        }
        if (root.right != null){
            preOrder(root.right);
        }
    }

    /**中序遍历 顺序：左，根，右 **/
    public void inOrder(Node root) {
        if (root == null){
            return;
        }
        if (root.left != null){
            inOrder(root.left);
        }
        System.out.println(root.value);
        if (root.right != null){
            inOrder(root.right);
        }
    }


    /**
     *    后序遍历 顺序：左，右，根
     */
    public void postOrder(Node root){
        if (root == null){
            return;
        }
        if (root.left != null){
            postOrder(root.left);
        }
        if (root.right != null){
            postOrder(root.right);
        }
        System.out.println(root.value);
    }

    /**
     *  获取二分搜索树中最小的节点
     */
    public Node minimum(Node node){
        if (node.left == null){
            return node;
        }
        return minimum(node.left);
    }

    //删除二分搜索树中最小的节点,并返回删除的节点
    public Node removeMin(){
        Node minNode = minimum(root);
        root = removeMin(root);
        return minNode;
    }
    /**
     * 删除二分搜索树中最小的节点,并返回根节点
     */
    public Node removeMin(Node node){
        if (node.left == null){
            Node rightNode = node.right;
            node.right = null;
            size--;
            return rightNode;
        }
        node.left = removeMin(node.left);
        return node;
    }

    /**
     * 获取二分搜索树中最大的节点
     */

    public Node maximum(Node node){
        if (node.right == null){
            return node;
        }
        return maximum(node.right);
    }

    /**
     * 删除二分搜索树中最大的节点，并返回根节点
     */
    public Node removeMax(Node node){
        //递归的终止条件
        if (node.right == null){
            Node leftNode = node.left;
            node.left = null;
            size--;
            return leftNode;
        }
        node.right = removeMax(node.right);
        return node;
    }

    /**
     * 删除指定节点的元素
     */
    public void remove(E e){

    }

    /**
     * 判断二分搜索树中是否包含某个节点
     * @param node
     * @param e
     * @return
     */
    public boolean contains(Node node, E e){
        //递归的终止条件
        if (node == null){
            return false;
        }
        if (e.compareTo(node.value) == 0){
            return true;
        } else if (e.compareTo(node.value) < 0){
            return contains(node.left, e);
        }else{
            return contains(node.right, e);
        }
    }


    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    //遍历二分搜索树
    @Override
    public String toString(){
        StringBuilder res = new StringBuilder();
        //前序遍历
//        preOrder(root);
        //中序遍历
        inOrder(root);
        //后序遍历
//        postOrder(root);
        return res.toString();
    }


    public static void main(String[] args) {
        BST2<Integer> bst2 = new BST2<>();
        bst2.add(5);
        bst2.add(3);
        bst2.add(7);
        bst2.add(1);
        bst2.add(4);
        bst2.add(2);
        bst2.add(6);
        System.out.println("=============前序遍历===========");
        bst2.preOrder(bst2.root);
        System.out.println("=============中序遍历===========");
        bst2.inOrder(bst2.root);
        System.out.println("=============后序遍历===========");
        bst2.postOrder(bst2.root);
        System.out.println(bst2.contains(bst2.root, 63));
        System.out.println(bst2.contains(bst2.root, 64));
        System.out.println(bst2.contains(bst2.root, 1));

        System.out.println("=============非递归方式遍历===========");
        bst2.preOrderNR();
        System.out.println("=============层序方式遍历===========");
        bst2.levelOrder(bst2.root);
        System.out.println("=============最小节点===========");
        System.out.println(bst2.minimum(bst2.root).value);
        System.out.println("=============最大节点===========");
        System.out.println(bst2.maximum(bst2.root).value);
        System.out.println("========================");


        //随机生成10000个数，放入到二分搜索树中
        BST2<Integer> bstTest = new BST2<>();
        ArrayList<Integer> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            bstTest.add(random.nextInt(10000));
        }

        while (!bstTest.isEmpty()){
            list.add(bstTest.removeMin().value);
        }
        System.out.println(list);
        System.out.println(list.size());

        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) - list.get(i-1)<0){
                throw new IllegalArgumentException("error");
            }
        }
        System.out.println("编译完成");
    }
}