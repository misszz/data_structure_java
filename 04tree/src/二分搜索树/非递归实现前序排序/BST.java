package 二分搜索树.非递归实现前序排序;

import java.util.Stack;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 二分搜索树中节点之间是有可比较性的
 * @since 2020/11/28 14:14
 **/

public class BST<E extends Comparable<E>> {

    class Node {
        E e;
        Node left, right;

        public Node(E e) {
            this.e = e;
            left = null;
            right = null;
        }
    }

    Node root;
    int size;

    public BST() {
        root = null;
        size = 0;
    }

    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }
    //向二分搜索树中添加元素
    public void add(E e) {
        if (root == null) {
            root = new Node(e);
            size ++ ;
        } else {
            add(root, e);
        }
    }

    private void add(Node node, E e) {

        if (e.compareTo(node.e) == 0){
            return;
        }else if (e.compareTo(node.e) < 0 && node.left == null) {
            node.left = new Node(e);
            size++;
        }else if (e.compareTo(node.e) > 0 && node.right == null) {
            node.right = new Node(e);
            size++;
        }

        if (e.compareTo(node.e) < 0){
            add(node.left,e);
        }else {
            add(node.right,e);
        }
    }

    public boolean contains(E e){
        return contains(root,e);
    }

    private boolean contains(Node node, E e) {
        //递归的终止条件
        if (node == null){
            return false;
        }

        if (e.compareTo(node.e) == 0 ){
            return true;
        }else if (e.compareTo(node.e) < 0){
            return contains(node.left, e);
        }else{
            return contains(node.right, e);
        }
    }

    //前序排序
    public void preOrder(){
        preOrder(root);
    }

    private void preOrder(Node node) {
        if (node == null){
            return;
        }
        System.out.println(node.e);
        preOrder(node.left);
        preOrder(node.right);
    }

    //中序排序
    public void inOrder(){
        inOrder(root);
    }

    private void inOrder(Node node) {
        if (node == null){
            return;
        }
        inOrder(node.left);
        System.out.println(node.e);
        inOrder(node.right);
    }

    //后序排序
    public void postOrder(){
        postOrder(root);
    }

    private void postOrder(Node node) {
        if (node == null){
            return;
        }
        postOrder(node.left);
        postOrder(node.right);
        System.out.println(node.e);
    }

    //非递归实现前序排序
    public void preOrderNR(){
        preOrderNR(root);
    }

    private void preOrderNR(Node root) {
        //使用栈的方式
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()){
            Node cur = stack.pop();
            System.out.println(cur.e);
            if (cur.right != null){
                stack.push(cur.right);
            }
            if (cur.left != null){
               stack.push(cur.left);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        generatedDeptString(root,0,res);
        return res.toString();
    }

    //生成以node为节点，深度为depth的描述二叉树的字符串
    private void generatedDeptString(Node node, int depth, StringBuilder res) {
        if (node == null){
            res.append(generatedDeptString(depth)+"null\n");
            return;
        }
        res.append(generatedDeptString(depth)+node.e+"\n");
        generatedDeptString(node.left, depth+1, res);
        generatedDeptString(node.right, depth+1, res);
    }

    private String generatedDeptString(int depth){
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < depth; i++) {
            res.append("--");
        }
        return res.toString();
    }

    public static void main(String[] args) {
        BST<Integer> bst = new BST<>();
        bst.add(3);
        bst.add(2);
        bst.add(1);
        bst.add(4);

//        boolean b = bst.contains(6);
//        System.out.println("b = " + b);
//
//        bst.preOrder();
//        System.out.println();
//        bst.inOrder();
//        System.out.println();
//        bst.postOrder();
//        System.out.println(bst.toString());

        bst.preOrderNR();


    }







}