package 二分搜索树;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 二分搜索树中节点之间是有可比较性的
 * @since 2020/11/28 14:14
 **/

public class BST1<E extends Comparable<E>> {

    class Node {
        E e;
        Node left, right;

        public Node(E e) {
            this.e = e;
            left = null;
            right = null;
        }
    }

    Node root;
    int size;

    public BST1() {
        root = null;
        size = 0;
    }

    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    //向二分搜索树中添加元素
    public void add(E e) {
        add(root, e);
    }

    private Node add(Node node, E e) {
        if (node == null){
            size++;
            return node = new Node(e);
        }

        if (e.compareTo(node.e) < 0){
            node.left = add(node.left,e);
        }else if (e.compareTo(node.e) > 0){
            node.right = add(node.right, e);
        }
        return node;
    }


    public static void main(String[] args) {
        BST1<Integer> bst = new BST1<>();
        bst.add(3);
        bst.add(2);
        bst.add(1);
        bst.add(4);


        System.out.println(bst);
    }
}
