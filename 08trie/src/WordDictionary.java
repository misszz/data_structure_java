import java.util.Map;
import java.util.TreeMap;

/**
 * @author wangzhongxu
 * *@description 211. 添加与搜索单词 - 数据结构设计
 * @since 2021/2/6 20:00
 **/

//请你设计一个数据结构，支持 添加新单词 和 查找字符串是否与任何先前添加的字符串匹配 。
//
//        实现词典类 WordDictionary ：
//
//        WordDictionary() 初始化词典对象
//        void addWord(word) 将 word 添加到数据结构中，之后可以对它进行匹配
//        bool search(word) 如果数据结构中存在字符串与 word 匹配，则返回 true ；否则，返回  false 。word 中可能包含一些 '.' ，每个 . 都可以表示任何一个字母。
//
//        来源：力扣（LeetCode）
//        链接：https://leetcode-cn.com/problems/design-add-and-search-words-data-structure
//        著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。

class WordDictionary {
    class Node{
        boolean isWord;
        Map<Character,Node> next;

        public Node(boolean isWord) {
            this.isWord = isWord;
            this.next = new TreeMap<>();
        }
        public Node() {
            this(false);
        }
    }

    Node root;
    int size;

    /** Initialize your data structure here. */
    public WordDictionary() {
        root = new Node(false);
        size = 0;
    }

    public void addWord(String word) {
        Node cur = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (cur.next.get(c) == null){
                cur.next.put(c, new Node());
            }
            cur = cur.next.get(c);
        }
        cur.isWord = true;
    }

    public boolean search(String word) {
        return match(root,word,0);
    }

    private boolean match(Node root, String word, int index) {
        if (index == word.length()){
            return root.isWord;
        }
        char c = word.charAt(index);
        if (c != '.'){
            if (root.next.get(c) == null){
                return false;
            }
            return match(root.next.get(c),word,index+1);
        }else {
            for (Character character : root.next.keySet()) {
                if (match(root.next.get(character),word,index+1)){
                    return true;
                }
            }
            return false;
        }
    }

}