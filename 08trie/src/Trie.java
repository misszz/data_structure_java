import java.util.Map;
import java.util.TreeMap;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 字典树·
 * @since 2021/2/6 14:57
 **/

public class Trie {
    class Node{
        boolean isWord;
        Map<Character,Node> next;   //每个节点有多个指向下个节点的指针

        public Node(boolean isWord){
            this.isWord = isWord;
            this.next = new TreeMap<>();
        }

        public Node(){
            this(false);
        }
    }

    public Node root;
    public int size;

    public Trie(){
        root = new Node();
        size = 0;
    }
    //获取trie中存储的单词个数
    public int getSize(){
        return size;
    }

    //往trie中添加单词
    public void add(String word){
        if (word == null || word.length() <= 0){
            throw new IllegalArgumentException("word is empty");
        }

        Node cur = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (cur.next.get(c) == null){
                cur.next.put(c, new Node());
            }
            cur = cur.next.get(c);
        }
        if (!cur.isWord){
            cur.isWord = true;
            size++;
        }
    }

    // 查询单词word是否在Trie中
    public boolean contains(String word){
        Node cur = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (cur.next.get(c) == null){
                return false;
            }
            root = cur.next.get(c);
        }
        return cur.isWord;
    }

    //前缀查询 查询在trie中是否有单词是以prefix为前缀的
    public boolean isPrefix(String prefix){
        Node cur = root;    //根节点不存储单词
        for (int i = 0; i < prefix.length(); i++) {
            char c = prefix.charAt(i);
            if (cur.next.get(c) == null){
                return false;
            }
            cur = root.next.get(c);
        }
        return true;
    }

}
