package com.linked.链表;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/11/17 13:23
 **/

public class LinkedList<E> {

    private Node head;  //记录头节点
    private int size;   //节点个数

    class Node {
        E e;
        Node next;

        public Node(E e,Node node){
            this.e = e;
            this.next = node;
        }
        public Node(E e){
            this(e,null);
        }
        public Node(){
            this(null,null);
        }
        public String toString(){
            return e.toString();
        }
    }
    public LinkedList(){
        head = null;
        size = 0;
    }

    public void addFirst(E e){
        Node node = new Node(e);
        node.next = head;
        head = node;
        size++;
    }
    // 在链表的index(0-based)位置添加新的元素e
    // 在链表中不是一个常用的操作，练习用：）
    public void add(int index ,E e){
        if (index < 0 || index > size){
            throw new IllegalArgumentException("下标越界");
        }
        if (index == 0){
            addFirst(e);
        }else {
            Node pre = head;
            Node node = new Node(e);
            for (int i = 0; i < index - 1 ; i++){
                pre = pre.next;
            }
            node.next  = pre.next;
            pre.next = node;
            size ++;
        }
    }

    public void addLast(E e){
        add(size, e);
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("linkedList : size: %d\n",size));
        sb.append("head[");
        Node head = this.head;
        for (int i = 0; i < size; i++) {
            sb.append(head.e);
            if (i != size - 1){
                sb.append(",");
            }
            head = head.next;
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.addLast("a");
        linkedList.addLast("b");
        linkedList.addLast("c");

        linkedList.addFirst("d");
        System.out.println(linkedList);
    }
}
