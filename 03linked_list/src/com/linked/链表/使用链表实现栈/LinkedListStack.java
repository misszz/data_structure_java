package com.linked.链表.使用链表实现栈;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 采用链表的方式实现栈
 * @since 2020/11/19 22:07
 **/

public class LinkedListStack <E> implements Stack<E>{

    private LinkedList<E> linkedList;

    public LinkedListStack(){
        linkedList = new LinkedList<>();
    }

    @Override
    public void push(E e) {
        linkedList.addLast(e);
    }

    @Override
    public void pop() {
        linkedList.removeLast();
    }

    @Override
    public E peek() {
        return linkedList.getLast();
    }

    @Override
    public int getSize() {
        return linkedList.getSize();
    }

    @Override
    public boolean isEmpty() {
        return linkedList.getSize() == 0;
    }
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Stack: size: %d\n",linkedList.getSize()));
        sb.append("[");
        for (int i = 0; i < linkedList.getSize(); i++) {
            sb.append(linkedList.get(i));
            if (i != linkedList.getSize() - 1){
                sb.append(",");
            }
        }
        sb.append("] top");
        return sb.toString();
    }

    public static void main(String[] args) {
        LinkedListStack<String> linkedListStack = new LinkedListStack<>();
        linkedListStack.push("a");
        linkedListStack.push("b");
        linkedListStack.push("c");
        System.out.println(linkedListStack.toString());
        System.out.println(linkedListStack.getSize());
        System.out.println(linkedListStack.isEmpty());
//        linkedListStack.pop();
        System.out.println(linkedListStack.peek());
        linkedListStack.pop();
        System.out.println(linkedListStack.peek());
//        linkedListStack.pop();
        System.out.println(linkedListStack.toString());
        System.out.println(linkedListStack.isEmpty());
    }
}
