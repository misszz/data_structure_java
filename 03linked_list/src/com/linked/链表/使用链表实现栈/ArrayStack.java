package com.linked.链表.使用链表实现栈;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description  基于数组的栈实现
 * @since 2020/11/14 13:03
 **/

public class ArrayStack<E> implements Stack<E>{
    private Array<E> array = new Array<>();

    @Override
    public void push(E e) {
        array.addFirst(e);
    }

    @Override
    public void pop() {
        array.removeLast();
    }

    @Override
    public E peek() {
        return array.get(array.getSize()-1);
    }

    @Override
    public int getSize() {
        return array.getSize();
    }

    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("stack: [");
        for (int i = 0; i < array.getSize(); i++) {
            sb.append(array.get(i));
            if (i != array.getSize() - 1){
                sb.append(",");
            }
        }
        sb.append("] top");
        return sb.toString();
    }
}
