package com.linked.链表.使用链表实现栈;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/11/14 16:20
 **/

public class Main {

    public static void main(String[] args) {
        Stack<Integer> arrayStack = new ArrayStack<>();
        arrayStack.push(1);
        arrayStack.push(2);
        arrayStack.push(3);
        System.out.println(arrayStack);
        System.out.println(arrayStack.getSize());
        System.out.println(arrayStack.isEmpty());
        arrayStack.pop();
        System.out.println(arrayStack);
        System.out.println(arrayStack.peek());
        arrayStack.pop();
        arrayStack.pop();
        System.out.println(arrayStack.isEmpty());

        
    }

}
