package com.linked.链表.使用链表实现栈;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/11/17 13:23
 **/

public class LinkedList<E> {

    private Node dummyHead;  //虚拟头节点
    private int size;   //节点个数

    class Node {
        E e;
        Node next;

        public Node(E e,Node node){
            this.e = e;
            this.next = node;
        }
        public Node(E e){
            this(e,null);
        }
        public Node(){
            this(null,null);
        }
        @Override
        public String toString(){
            return e.toString();
        }
    }

    public LinkedList(){
        dummyHead = new Node(null, null);
        size = 0;
    }

    public int getSize(){
        return size;
    }

    public void addFirst(E e){
        add(0, e);
    }
    // 在链表的index(0-based)位置添加新的元素e
    // 在链表中不是一个常用的操作，练习用：）
    public void add(int index ,E e){
        if (index < 0 || index > size){
            throw new IllegalArgumentException("下标越界");
        }
        Node pre = dummyHead;
        Node node = new Node(e);
        for (int i = 0; i < index; i++){
            pre = pre.next;
        }
        node.next  = pre.next;
        pre.next = node;
        size ++;
    }

    public void addLast(E e){
        add(size, e);
    }

    public E get(int index){
        if (index < 0 || index >= size){
            throw new RuntimeException("下标越界");
        }
        Node cur = dummyHead.next;
        for (int i = 0; i < index; i++) {
            cur = cur.next;
        }
        return cur.e;
    }

    public E getFirst(){
        return get(0);
    }

    public E getLast(){
        return get(size - 1);
    }

    //修改指定索引位置的元素
    public void set(int index,E e){
        if (index < 0 || index >= size){
            throw new RuntimeException("下标越界");
        }
        Node cur = dummyHead.next;
        for (int i = 0; i < index; i++) {
            cur = cur.next;
        }
        cur.e  = e;
    }

    //查找是否包含某个元素
    public boolean contains(E e){
        Node cur = dummyHead.next;
        for (int i = 0; i < size; i++) {
            if (cur.e.equals(e)){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    //删除指定索引位置的元素
    public E remove(int index){
        Node pre = dummyHead;
        for (int i = 0; i < index; i++) {
            pre = pre.next;
        }
        Node delNode = pre.next;
        pre.next = delNode.next;
        delNode.next = null;
        size--;
        return delNode.e;
    }

    public E removeFirst(){
        return remove(0);
    }

    public E removeLast(){
        return remove(size - 1);
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("linkedList : size: %d\n",size));
        sb.append("head [");
        Node head = this.dummyHead;
        for (int i = 0; i < size; i++) {
            sb.append(head.next.e);
            if (i != size - 1){
                sb.append("-->");
            }
            head = head.next;
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.addLast("a");
        System.out.println(linkedList);
        linkedList.addLast("b");
        System.out.println(linkedList);
        linkedList.addLast("c");
        System.out.println(linkedList);
        linkedList.addLast("e");
        System.out.println(linkedList);
        linkedList.addFirst("d");
        System.out.println(linkedList);
        System.out.println(linkedList.get(3));

        System.out.println(linkedList.contains("d"));
        String removeEle = linkedList.remove(2);
        System.out.println("removeEle = " + removeEle);
        System.out.println(linkedList);
    }
}
