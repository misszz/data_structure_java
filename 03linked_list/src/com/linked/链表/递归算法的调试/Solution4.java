package com.linked.链表.递归算法的调试;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

//删除链表中等于给定值 val 的所有节点。
//
//        示例:
//
//        输入: 1->2->6->3->4->5->6, val = 6
//        输出: 1->2->3->4->5

    //采用递归方式简化代码


class Solution4 {
    public ListNode removeElements(ListNode head, int val ,int depth) {
        String depthString = generateDepthString(depth);
        System.out.print(depthString);
        System.out.println("Call :remove :"+val + " in " + head);

        if (head == null){
            System.out.print(depthString);
            System.out.println("Return: "+ head);
            return null;
        }
        ListNode res = removeElements(head.next, val ,depth + 1);
        System.out.print(depthString);
        System.out.println("After remove "+ val + ": "+res);

        ListNode ret;
        if (head.val == val){
            ret = res;
        }else {
            head.next = res;
            ret = head;
        }
        System.out.print(depthString);
        System.out.println("Return: " + ret);
        return ret;
    }

    private String generateDepthString(int depth) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ;i <depth ;i ++){
            sb.append("--");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
//        1->2->6->3->4->5->6, val = 6
        int[] arr = {1,2,6,3,4,5,6};
        ListNode node = new ListNode(arr);
        System.out.println(node);


        ListNode listNode = new Solution4().removeElements(node, 6 , 0);
        System.out.println(listNode);
    }

}