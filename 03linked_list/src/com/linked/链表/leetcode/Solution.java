package com.linked.链表.leetcode;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

//删除链表中等于给定值 val 的所有节点。
//
//        示例:
//
//        输入: 1->2->6->3->4->5->6, val = 6
//        输出: 1->2->3->4->5

class Solution {

    public static ListNode globalHead;
    public ListNode removeElements(ListNode head, int val) {
        //如果头节点不为空 并且头节点元素的值就是要删除的元素
        while (head !=null && head.val == val){
            ListNode delNode = head;
            head = head.next;
            delNode.next = null;
        }

        if (head == null){
            return head;
        }

        //删除链表中节点时要记录删除节点的上一个元素
        ListNode pre = head;
        while (pre.next != null){
            if (pre.next.val == val){
                //记录要删除的节点
                ListNode delNode = pre.next;
                pre.next = delNode.next;
                delNode.next = null;
            }else {
                pre = pre.next;
            }
        }
        return head;
    }

    @Override
    public String toString(){
        StringBuilder sb= new StringBuilder();
        sb.append("linkedList [");
        ListNode cur = globalHead;
        while (cur != null){
            sb.append(cur.val);
            if (cur.next != null){
                sb.append("->");
            }
            cur = cur.next;
        }
        sb.append("]");
        return sb.toString();
    }



    public static void main(String[] args) {
//        1->2->6->3->4->5->6, val = 6
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(6);
        ListNode node4 = new ListNode(3);
        ListNode node5 = new ListNode(4);
        ListNode node6 = new ListNode(5);
        ListNode node7 = new ListNode(6);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;
        node6.next = node7;

        globalHead = node1;
        Solution solution = new Solution();
        System.out.println(solution);
        ListNode listNode = solution.removeElements(node1, 6);
        System.out.println("listNode = " + listNode);
        System.out.println(solution);
    }
}