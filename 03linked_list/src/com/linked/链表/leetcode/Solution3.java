package com.linked.链表.leetcode;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

//删除链表中等于给定值 val 的所有节点。
//
//        示例:
//
//        输入: 1->2->6->3->4->5->6, val = 6
//        输出: 1->2->3->4->5

    //采用递归方式简化代码


class Solution3 {
    public ListNode removeElements(ListNode head, int val) {
        if (head == null){
            return null;
        }
        head.next = removeElements(head.next, val);
        return head.val == val ? head.next : head;
    }

    public static void main(String[] args) {
//        1->2->6->3->4->5->6, val = 6
        int[] arr = {1,2,6,3,4,5,6};
        ListNode node = new ListNode(arr);
        System.out.println(node);


        ListNode listNode = new Solution3().removeElements(node, 6);
        System.out.println(listNode);
    }

}