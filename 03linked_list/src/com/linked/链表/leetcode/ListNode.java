package com.linked.链表.leetcode;

public class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }

    ListNode(int[] arr){
        //1->2->6->3->4->5->6
        this.val = arr[0];
        ListNode cur = this;

        for (int i = 0; i < arr.length - 1; i++) {
            cur.next = new ListNode(arr[i + 1]);
            cur = cur.next;
        }
    }
    @Override
    public String toString(){
        ListNode cur = this;
        StringBuilder sb = new StringBuilder();
        sb.append("listNode [");
        while (cur != null){
            sb.append(cur.val);
            if (cur.next !=null){
                sb.append("->");
            }
            cur = cur.next;
        }
        sb.append("]");
        return sb.toString();
    }
}
