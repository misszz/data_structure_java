package com.linked.链表.leetcode;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/11/21 15:04
 **/

//递归求一个数组的和 {1,2,3,4,5}
public class Sum {

    public int sum(int[] arr){
        return sum(arr,0);
    }

    private int sum(int[] arr, int l) {
        if (arr.length == l){
            return 0;
        }
        return arr[l] + sum(arr,l + 1);
    }

    public static void main(String[] args) {
        int sum = new Sum().sum(new int[]{1, 2, 3, 4, 5, 6});
        System.out.println("sum = " + sum);
    }
}
