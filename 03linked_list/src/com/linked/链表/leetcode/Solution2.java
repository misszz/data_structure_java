package com.linked.链表.leetcode;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

//删除链表中等于给定值 val 的所有节点。
//
//        示例:
//
//        输入: 1->2->6->3->4->5->6, val = 6
//        输出: 1->2->3->4->5

    //采用虚拟头节点的方式简化代码
class Solution2 {
    public ListNode removeElements(ListNode head, int val) {
        ListNode dummyHead = new ListNode(-1);
        dummyHead.next = head;
        //删除链表中节点时要记录删除节点的上一个元素
        ListNode pre = dummyHead;
        while (pre.next != null){
            if (pre.next.val == val){
                //记录要删除的节点
                ListNode delNode = pre.next;
                pre.next = delNode.next;
                delNode.next = null;
            }else {
                pre = pre.next;
            }
        }
        return dummyHead.next;
    }

    public static void main(String[] args) {
//        1->2->6->3->4->5->6, val = 6
        int[] arr = {1,2,6,3,4,5,6};
        ListNode node = new ListNode(arr);
        System.out.println(node);

        ListNode listNode = new Solution2().removeElements(node, 6);
        System.out.println(listNode);
    }

}