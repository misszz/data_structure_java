package com.linked.链表.使用链表实现队列;


/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/11/19 22:33
 **/

public class LinkedListQueue<E> implements Queue<E>{

    private LinkedList<E> linkedList;

    public LinkedListQueue(){
        linkedList = new LinkedList<>();
    }

    @Override
    public void enqueue(E e) {
        linkedList.addLast(e);
    }

    @Override
    public E dequeue() {
        return linkedList.removeFirst();
    }

    @Override
    public E getFront() {
        return linkedList.getFirst();
    }

    @Override
    public int getSize() {
        return linkedList.getSize();
    }

    @Override
    public boolean isEmpty() {
        return linkedList.getSize() == 0;
    }

    @Override
    public String toString(){
        return linkedList.toString();
    }


    public static void main(String[] args) {
        LinkedListQueue<String> queue = new LinkedListQueue<>();
        queue.enqueue("a");
        queue.enqueue("c");
        queue.enqueue("c");
        queue.enqueue("d");
        System.out.println(queue);
        System.out.println("queue.getFront() = " + queue.getFront());
        System.out.println(queue.getSize());
        System.out.println(queue.isEmpty());
        System.out.println("queue.dequeue() = " + queue.dequeue());
        System.out.println(queue.getSize());
        System.out.println("queue.dequeue() = " + queue.dequeue());
        System.out.println("queue.dequeue() = " + queue.dequeue());
        System.out.println("queue.dequeue() = " + queue.dequeue());
        System.out.println(queue.isEmpty());

    }
}
