/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2021/2/5 15:16
 **/

class NumArray2 {
    public Integer[] sum;   // sum[i]存储前i个元素和, sum[0] = 0
                            // 即sum[i]存储nums[0...i-1]的和
                            // sum(i, j) = sum[j + 1] - sum[i]
    public NumArray2(int[] nums) {
        sum = new Integer[nums.length + 1];
        sum[0] = 0;
        for (int i = 0; i < nums.length; i++) {
            sum[i + 1] = nums[i] + sum[i];
        }
    }

    /**
     * 实际求 区间【i。。。j】的元素和 sum【j】 - sum【i - 1】
     *
     * @param i
     * @param j
     * @return
     */
    public int sumRange(int i, int j) {
        //sumRange(2,3)  寓意： nums[0,3] - mums[0,1]  因为第一个元素sum[0] = 0  所以为sum[4] - sum[2]
        return sum[j + 1] - sum[i];
    }


    public static void main(String[] args) {
        int[] nums = {-2, 0, 3, -5, 2, -1};
        NumArray2 numArray = new NumArray2(nums);
        System.out.println(numArray.sumRange(0, 3));
        System.out.println(numArray.sumRange(1, 3));
        System.out.println(numArray.sumRange(2, 3));
        System.out.println(numArray.sumRange(3, 3));
    }
}

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray obj = new NumArray(nums);
 * int param_1 = obj.sumRange(i,j);
 */