/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2021/2/5 14:54
 **/

class NumArray {
    private SegementTree<Integer> segementTree;

    public interface Merger<E> {
        E merge(E a,E b);
    }

    public class SegementTree<E> {
        E[] data;
        E[] tree;
        Merger<E> merger;

        //初始化数组和线段树
        public SegementTree(E[] arrs, Merger<E> merger) {
            this.merger = merger;
            this.data = (E[]) new Object[arrs.length];
            this.tree = (E[]) new Object[arrs.length * 4];   //构建线段树
            for (int i = 0; i < arrs.length; i++) {
                data[i] = arrs[i];
            }
            buildSegementTree(0, 0, data.length - 1);
        }

        //在treeIndex的位置创建区间[l...r]的线段树
        private void buildSegementTree(int treeIndex, int l, int r) {
            if (l == r) {
                tree[treeIndex] = data[l];
                return;
            }
            int leftIndex = leftChild(treeIndex);
            int rightIndex = rightChild(treeIndex);
            int mid = l + (r - l) / 2;   //防止整形溢出
            buildSegementTree(leftIndex, l, mid);
            buildSegementTree(rightIndex, mid + 1, r);
            tree[treeIndex] = merger.merge(tree[leftIndex], tree[rightIndex]);
        }

        // 返回区间[queryL, queryR]的值
        public E query(int queryL,int queryR){
            if (queryL > queryR || queryL < 0 || queryR >= data.length) {
                throw new IllegalArgumentException("index is illegal");
            }
            return query(0,0,data.length - 1,queryL,queryR);
        }

        private E query(int treeIndex, int l, int r, int queryL, int queryR) {
            if (l == queryL && r == queryR){
                return tree[treeIndex];
            }
            int leftIndex = leftChild(treeIndex);
            int rightIndex = rightChild(treeIndex);

            int mid = l + (r - l) / 2;

            if (queryR <= mid){
                return query(leftIndex, l, mid, queryL, queryR);
            }else if (queryL > mid){
                return query(rightIndex,mid+1, r, queryL, queryR);
            }
            E leftValue = query(leftIndex, l, mid, queryL, mid);
            E rightValue = query(rightIndex,mid+1, r, mid+1, queryR);
            return merger.merge(leftValue, rightValue);
        }

        public E get(int index) {
            if (index < 0 || index > data.length - 1) {
                throw new IllegalArgumentException("index is illegal");
            }
            return data[index];
        }

        public int getSize() {
            return data.length;
        }

        //判断是否为空
        public boolean isEmpty() {
            return data.length == 0;
        }

        public int leftChild(int index) {
            return 2 * index + 1;
        }

        public int rightChild(int index) {
            return 2 * index + 2;
        }

        @Override
        public String toString() {
            StringBuilder res = new StringBuilder();
            res.append('[');
            for (int i = 0; i < tree.length; i++) {
                res.append(tree[i]);
                if (i != tree.length - 1) {
                    res.append(',');
                }
            }
            res.append(']');
            return res.toString();
        }
    }

    public NumArray(int[] nums) {
        //int类型的数组是不能直接转换成integer类型的数组的
        if (nums.length > 0){
            Integer[] nu = new Integer[nums.length];
            for (int i = 0; i < nums.length; i++) {
                nu[i] = nums[i];
            }
            segementTree = new SegementTree<Integer>(nu, ((a, b) -> a + b));
        }
    }

    public int sumRange(int i, int j) {
        return segementTree.query(i, j);
    }
}

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray obj = new NumArray(nums);
 * int param_1 = obj.sumRange(i,j);
 */