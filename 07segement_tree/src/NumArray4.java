import java.util.Arrays;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description   力扣上 307题 区域和检索 - 数组可修改
 * @since 2021/2/5 15:16
 **/

class NumArray4 {
    public Integer[] sum;   // sum[i]存储前i个元素和, sum[0] = 0
                            // 即sum[i]存储nums[0...i-1]的和
                            // sum(i, j) = sum[j + 1] - sum[i]

    private int[] data;
    public NumArray4(int[] nums) {
        data = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            data[i] = nums[i];
        }

        sum = new Integer[nums.length + 1];
        sum[0] = 0;
        for (int i = 1; i < sum.length; i++) {
            sum[i] = nums[i - 1] + sum[i - 1];
        }
    }

    public void update(int index, int val) {
        data[index] = val;
//        buildSumArr(data);
        for (int i = index; i < data.length; i++) {
            sum[i + 1] = data[i] + sum[i];
        }
    }

    public int sumRange(int left, int right) {//寓意： nums[0,3] - mums[0,1]  因为第一个元素sum[0] = 0  所以为sum[4] - sum[2]
        return sum[right + 1] - sum[left];
    }

    public static void main(String[] args) {
        int[] nums = {-2, 0, 3, -5, 2, -1};
        NumArray4 numArray = new NumArray4(nums);
        System.out.println(numArray.sumRange(0, 3));
        System.out.println(numArray.sumRange(1, 3));
        System.out.println(numArray.sumRange(2, 3));
        System.out.println(numArray.sumRange(3, 3));
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        numArray.update(0, -1);
        System.out.println(Arrays.toString(numArray.data));
        System.out.println(numArray.sumRange(0, 3));
        System.out.println(numArray.sumRange(1, 3));
        System.out.println(numArray.sumRange(2, 3));
        System.out.println(numArray.sumRange(3, 3));
    }
}

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray obj = new NumArray(nums);
 * int param_1 = obj.sumRange(i,j);
 */