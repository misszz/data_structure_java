package com.queue.循环队列;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/11/15 0:55
 **/

public class LoopQueue<E> implements Queue<E> {
    private E[] data;
    private int font, tail;
    private int size;

    public LoopQueue(int capacity) {
        data = (E[]) new Object[capacity + 1];      //空留出来一个位置
        font = 0;
        tail = 0;
        size = 0;
    }

    public LoopQueue() {
        this(10);
    }

    public int getCapacity(){
        return data.length - 1;
    }

    @Override
    public void enqueue(E e) {
        if ((tail + 1) % data.length == font) {
            //动态扩容
            resize(getCapacity()*2);
        }
        data[tail] = e;
        tail = (tail + 1) % data.length;
        size ++;
    }

    private void resize(int newCapacity) {
        E[] e = (E[]) new Object[newCapacity+1];
        for (int i = 0; i < size; i++) {
            e[i] = data[(i + font) % data.length];
        }
        data = e;
        font = 0;
        tail = size;
    }

    @Override
    public E dequeue() {
        //动态缩容
        if (size < data.length/2){
            resize(getCapacity() * 3/4);
        }
        E e = data[font];
        data[font] = null;
        font = (font +1) % data.length;
        size--;
        return e;
    }

    @Override
    public E getFront() {
        return data[font];
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("loopQueue: size: %d ,capacity: %d\n", size ,getCapacity()));
        sb.append("font [");

        for (int i = 0; i < size; i++) {
            sb.append(data[(i + font) % data.length]);
            if (i != size - 1){
                sb.append(",");
            }
        }

        sb.append("] tail");
        return sb.toString();
    }

    public static void main(String[] args) {
        LoopQueue<Integer> loopQueue = new LoopQueue<>();
        for (int i = 0; i < 11; i++) {
            loopQueue.enqueue(i);
            System.out.println(loopQueue);
        }
        System.out.println("出队>>>>>>>>>>>>>>>>");
        for (int i = 0; i < 10; i++) {
            loopQueue.dequeue();
            System.out.println(loopQueue);
        }
    }
}
