package com.queue.数组队列;

/**
 * 队列接口
 * @param <E>
 */
public interface Queue<E> {
    void enqueue(E e);     //入队
    E dequeue();     //出队
    E getFront();   //查看队首
    int getSize();
    boolean isEmpty();
}
