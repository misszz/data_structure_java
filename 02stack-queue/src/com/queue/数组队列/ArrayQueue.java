package com.queue.数组队列;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/11/15 0:31  数组队列 先进先出
 **/

public class ArrayQueue<E> implements Queue<E>{
    private Array<E> array;

    public ArrayQueue() {
        array = new Array<>(10);
    }
    public ArrayQueue(int capacity) {
        array = new Array<>(capacity);
    }
    @Override
    public void enqueue(E e) {
        array.addLast(e);
    }

    @Override
    public E dequeue() {
        return array.removeFirst();
    }

    @Override
    public E getFront() {
        return array.get(0);
    }

    @Override
    public int getSize() {
        return array.getSize();
    }

    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("queue: size: %d,capacity: %d\n", array.getSize(),array.getCapacity()));
        sb.append("[");
        for (int i = 0; i < array.getSize();i++){
            sb.append(array.get(i));
            if (i != array.getSize() - 1){
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        Queue<String> queue = new ArrayQueue<>();
        queue.enqueue("11");
        queue.enqueue("22");
        queue.enqueue("33");
        queue.enqueue("44");
        queue.enqueue("55");
        System.out.println(queue);
        System.out.println(queue.getFront());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue);
        System.out.println(queue.getFront());
    }
}
