package com.stack;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/11/14 12:48
 **/

public interface Stack <E>{
    void push(E e);    //入栈
    void pop();     //出栈
    E peek();       //获取栈顶元素
    int getSize();
    boolean isEmpty();
}
