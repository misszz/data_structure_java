package com.stack.demo;

import java.util.Stack;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
 * <p>
 * 有效字符串需满足：
 * <p>
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 * 注意空字符串可被认为是有效字符串。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/valid-parentheses
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @since 2020/11/14 17:10
 **/

public class Example {
    public static void main(String[] args) {
        String str1 = "([{}])";
        String str2 = "{)(";
        String str3 = "{)(";
        String str4 = "(])";
        System.out.println(isValid(str1));
        System.out.println(isValid(str2));
        System.out.println(isValid(str3));
        System.out.println(isValid(str4));
    }

    public static boolean isValid(String str) {
        Stack<String> stack = new Stack<>();
        String[] split = str.split("");
        for (String s : split) {
            if ("(".equals(s) || "[".equals(s) || "{".equals(s)) {
                stack.push(s);
            }else {
                if (stack.isEmpty()){
                    return false;
                }
                String top = stack.pop();
                if (")".equals(s) && !"(".equals(top)){
                    return false;
                }
                if ("]".equals(s) && !"[".equals(top)){
                    return false;
                }
                if ("}".equals(s) && !"{".equals(top)){
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
