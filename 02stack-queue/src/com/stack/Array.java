package com.stack;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 包含 查找 和移除
 * @since 2020/11/13 17:25
 **/

public class Array<E> {
    private E[] data; //实际存放的数据
    private int size;   //实际的大小

    //构造函数,传入数组的容量构造函数
    public Array(int capacity) {
        data = (E[]) new Object[capacity];
        this.size = 0;
    }

    //无参构造,默认容量为10
    public Array() {
        this(10);
    }

    //获取数组的实际存储元素的大小
    public int getSize() {
        return this.size;
    }

    //获取数组的长度
    public int getCapacity() {
        return data.length;
    }

    //判断数组是否为空
    public boolean isEmpty() {
        return size == 0;
    }

    //在指定索引位置添加元素
    public void add(int index, E e) {
        dynamicArray(index);//动态扩容数组
        if (index < 0) {
            throw new RuntimeException("数组下标越界");
        }
        //以添加索引位置为界向后挪一个
        for (int i = size - 1; i >= index; i--) {
            data[i + 1] = data[i];
        }
        data[index] = e;
        size++;
    }

    private void dynamicArray(int index) {
        if (size == data.length) {
            E[] array = (E[]) new Object[size * 2];
            for (int i = 0; i < size; i++) {
                array[i]  = data[i];
            }
            data = array;
        }
    }

    //在首位添加元素
    public void addFirst(E e) {
        add(0, e);
    }

    //在末尾添加元素
    public void addLast(E e) {
        add(size, e);
    }

    //在指定索引位置删除元素,返回删除的元素
    public E remove(int index) {
        if (index >= data.length || index < 0) {
            throw new RuntimeException("数组下标越界");
        }
        E e = data[index];
        for (int i = index; i < size; i++) {
            data[i] = data[i + 1];
        }
        data[size - 1] = null;
        size--;
        return e;
    }

    public E removeFirst() {
        return remove(0);
    }

    public E removeLast() {
        return remove(size - 1);
    }

    //是否包含某个元素
    public boolean contain(E e) {
        for (E d : data) {
            if (e.equals(d)) {
                return true;
            }
        }
        return false;
    }

    //查找某个元素的索引位置
    public int find(E e) {
        for (int i = 0; i < size; i++) {
            if (data[i] == e) {
                return i;
            }
        }
        return -1;
    }

    //返回具体索引的元素
    public E get(int index) {
        if (index < 0 || index > data.length) {
            throw new RuntimeException("数组下标越界");
        }
        return data[index];
    }

    //更新索引位置的元素
    public void update(int index, E e) {
        if (index < 0 || index > data.length) {
            throw new RuntimeException("数组下标越界");
        }
        data[index] = e;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Array:size = %d,capacity =  %d\n", size, data.length));
        sb.append("[");
        for (int i = 0; i < data.length; i++) {
            sb.append(data[i]);
            if (i != data.length - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        Array<Integer> array = new Array<>();
        array.addLast(1);
        array.addLast(2);
        array.addLast(3);
        array.addLast(4);
        array.addLast(5);
        array.addLast(6);
        array.addLast(7);
        array.addLast(8);
        array.addLast(9);
        array.addLast(10);
        array.addLast(11);
        System.out.println(array);
//        System.out.println(array.contain(3));
//        System.out.println(array.contain(7));
        array.removeFirst();
//        array.removeLast();
        System.out.println(array);
        System.out.println(array.get(0));
        System.out.println(array.find(2));
    }
}

