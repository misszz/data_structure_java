package lindedListSet;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 基于链表实现的集合
 * @since 2021/1/20 10:22
 **/

public class LinkedListSet<E> implements Set<E> {
    private LinkedList<E> linkedList;
    public LinkedListSet(){
        linkedList = new LinkedList<>();
    }

    @Override
    public void add(E e) {
        if (!linkedList.contains(e)){
            linkedList.addFirst(e);
        }
    }

    @Override
    public boolean contains(E e) {
        return linkedList.contains(e);
    }

    @Override
    public void remove(E e) {
        linkedList.removeElement(e);
    }

    @Override
    public int getSize() {
        return linkedList.getSize();
    }

    @Override
    public boolean isEmpty() {
        return linkedList.isEmpty();
    }
}
