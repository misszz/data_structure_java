package lindedListSet;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2021/1/20 9:48
 **/

public interface Set<E>{
    void add(E e);
    boolean contains(E e);
    void remove(E e);
    int getSize();
    boolean isEmpty();
}
