package map.基于二分搜索树;

/**
 * @author wangzhongxu
 * @since 2021/2/8 17:00
 **/

public interface Map<K,V> {
    void add(K key,V value);
    boolean contains(K key);
    V remove(K key);
    V get(K key);
    void set(K key,V value);

    int getSize();
    boolean isEmpty();
}
