package map.基于二分搜索树;

/**
 * @author wangzhongxu
 * @since 2021/2/9 12:20
 **/

public class BSTMap<K extends Comparable<K>, V> implements Map<K, V> {
    public class Node {
        K key;
        V value;
        Node left, right;

        public Node(K key, V value, Node left, Node right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }

        public Node() {
            this(null, null, null, null);
        }

        public Node(K key, V value) {
            this(key, value, null, null);
        }
    }

    Node root;
    int size;

    public BSTMap() {
        root = null;
        size = 0;
    }

    @Override
    public void add(K key, V value) {
        root = addNode(root, key, value);
    }

    //添加键为key，值为value的新节点 并返回根节点
    private Node addNode(Node root, K key, V value) {
        if (root == null) {
            root = new Node(key, value);
            size++;
            return root;
        }
        if (key.compareTo(root.key) > 0) {
            root.right = addNode(root.right, key, value);
        } else if (key.compareTo(root.key) < 0) {
            root.left = addNode(root.left, key, value);
        } else if (key.compareTo(root.key) == 0) {
            root.value = value;
        }
        return root;
    }

    @Override
    public boolean contains(K key) {
        return contains(root, key);
    }

    private boolean contains(Node root, K key) {
        if (root == null) {
            return false;
        }
        if (root.key.compareTo(key) < 0) {
            return contains(root.right, key);
        } else if (root.key.compareTo(key) > 0) {
            return contains(root.left, key);
        } else {
            return true;
        }
    }

    //取出最小的节点
    public Node mini() {
        return miniNode(root);
    }

    public Node miniNode(Node root) {
        if (root.left == null) {
            return root;
        }
        return miniNode(root.left);
    }

    // 删除掉以node为根的二分搜索树中的最小节点
    // 返回删除节点后新的二分搜索树的根
    public Node removeMin() {
        root = removeMin(this.root);
        return root;
    }

    private Node removeMin(Node root) {
        //TODO
        if (root.left == null) {
            size--;
            return null;
        }
        root.left = removeMin(root.left);
        return root;
    }

    @Override
    public V remove(K key) {
        Node removeNode = remove(root, key);
        return removeNode.value;
    }

    private Node remove(Node root, K key) {
        if (root == null) {
            return null;
        }
        if (root.key.compareTo(key) > 0) {
            root.left = remove(root.left, key);
            return root;
        } else if (root.key.compareTo(key) < 0) {
            root.right = remove(root.right, key);
            return root;
        } else {//root.key.compareTo(key) == 0
            if (root.left == null) {
                Node rightNode = root.right;
                root.right = null;
                size--;
                return rightNode;
            }
            if (root.right == null) {
                Node leftNode = root.left;
                root.left = null;
                size--;
                return leftNode;
            }
            //如果当前节点的左右节点都不为空的情况
            Node miniNode = miniNode(root.right);   //查找当前节点右节点的最小节点
            Node removeMin = removeMin(root.right); //删除右节点删除后的新节点
            miniNode.left = root.left;
            miniNode.right = removeMin;

            root.left = root.right = null;
            return miniNode;
        }
    }

    @Override
    public V get(K key) {
        Node node = getNode(root, key);
        return node == null ? null : node.value;
    }

    private Node getNode(Node root, K key) {
        if (root == null) {
            return null;
        }
        if (root.key.compareTo(key) < 0) {
            return getNode(root.right, key);
        } else if (root.key.compareTo(key) > 0) {
            return getNode(root.left, key);
        } else {  //root.key.compareTo(key) == 0
            return root;
        }
    }

    @Override
    public void set(K key, V value) {
        Node node = getNode(root, key);
        if (node == null) {
            throw new RuntimeException(key + "not exits");
        } else {
            node.value = value;
        }
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
}