package map.基于链表;

import java.util.ArrayList;

/**
 * @author wangzhongxu
 * @since 2021/2/8 16:59
 * 基于链表实现的Map
 **/

public class LinkedListMap<K,V> implements Map<K,V>{

    public class Node{
        K key;
        V value;
        Node next;

        public Node(K key, V value, Node next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public Node(K key, V value) {
            this(key,value,null);
        }
        public Node(){
            this(null,null,null);
        }
    }

    public Node dummyHead;
    public int size;

    public LinkedListMap(){
        dummyHead = new Node();
        size = 0;
    }

    @Override
    public void add(K key, V value) {
        Node node = getNode(key);
        if (node == null){
            dummyHead.next = new Node(key, value, dummyHead.next);//简化版
            size++;
//            Node addNode = new Node(key,value);
//            Node temp = dummyHead.next;
//            dummyHead.next = addNode;
//            addNode.next = temp;
        }else {
            node.value = value;
        }
    }

    @Override
    public boolean contains(K key) {
        return getNode(key) != null;
    }

    @Override
    public V remove(K key) {
        Node cur = dummyHead;
        while (cur.next != null){
            if(cur.next.key.equals(key)){
//                cur.next = cur.next.next; 第一种方式
                Node del = cur.next;
                cur.next = del.next;
                del.next = null;
                size--;
                return del.value;
            }
            cur = cur.next;
        }
        return null;
    }

    @Override
    public V get(K key){
        Node node = getNode(key);
        return node == null ? null : node.value;
    }

    public Node getNode(K key) {
        Node cur = dummyHead.next;
        while (cur != null){
            if (cur.key.equals(key)){
                return cur;
            }
            cur = cur.next;
        }
        return null;
    }

    @Override
    public void set(K key, V value) {
        Node node = getNode(key);
        if (node == null){
            throw new IllegalArgumentException("node doesn't exists");
        }else {
            node.value = value;
        }
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    public static void main(String[] args) {
        System.out.println("Pride and Prejudice");
        ArrayList<String> words = new ArrayList<>();
        if(FileOperation.readFile("E:\\gitResposity\\data_structure_java\\05set-and-map\\pride-and-prejudice.txt", words)) {
            System.out.println("Total words: " + words.size());

            LinkedListMap<String, Integer> map = new LinkedListMap<>(); //记录每个单词出现的频率
            for (String word : words) {
                if (map.get(word) == null){
                    map.add(word, 1);
                }else {
                    map.add(word, map.get(word) + 1);
                }
            }

            System.out.println("Total different words: " + map.getSize());
            System.out.println("Frequency of PRIDE: " + map.get("pride"));
            System.out.println("Frequency of PREJUDICE: " + map.get("prejudice"));
        }

        System.out.println();
    }
}
