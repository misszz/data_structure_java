package leetcode;

import java.util.LinkedHashSet;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2021/1/20 19:46
 **/

class Solution {
    public static int uniqueMorseRepresentations(String[] words) {
        String[] rules = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();
//        TreeSet<String> linkedHashSet = new TreeSet<>();
        for (String word : words) {
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < word.length(); i++) {
                res.append(rules[word.charAt(i) - 'a']);
            }
            linkedHashSet.add(res.toString());
        }
        return linkedHashSet.size();
    }

    public static void main(String[] args) {
        System.out.println(Solution.uniqueMorseRepresentations(new String[]{"gin", "zen", "gig", "msg"}));
    }
}
