package bstset;

import lindedListSet.Set;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description  基于二分搜索树实现的Set 去除重复元素
 * @since 2021/1/20 9:53
 **/

public class BSTSet<E extends Comparable<E>> implements Set<E> {
    BST3<E> bst3 = null;
    public BSTSet(){
        bst3 = new BST3<>();
    }

    @Override
    public void add(E e) {
        bst3.add(e);
    }

    @Override
    public boolean contains(E e) {
        return bst3.contains(e);
    }

    @Override
    public void remove(E e) {
        bst3.remove(e);
    }


    @Override
    public int getSize() {
        return bst3.size;
    }

    @Override
    public boolean isEmpty() {
        return bst3.isEmpty();
    }
}
