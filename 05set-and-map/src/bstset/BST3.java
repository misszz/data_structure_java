package bstset;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2021/1/17 12:04
 **/

public class BST3<E extends Comparable<E>>{
    Node root;
    int size;
    class Node{
        E e;
        Node left,right;

        public Node(E e, Node left, Node right) {
            this.e = e;
            this.left = left;
            this.right = right;
        }
        public Node(E e){
            this(e,null,null);
        }
    }

    //添加节点,并返回根节点
    public void add(E e){
        root = addNode(root,e);
    }

    private Node addNode(Node root, E e) {
        if (root == null){
            root = new Node(e);
            size++;
        }

        if (e.compareTo(root.e) == 0){
            return root;
        }else if (e.compareTo(root.e) > 0){
            root.right = addNode(root.right,e);
            return root;
        }else{
            root.left = addNode(root.left, e);
            return root;
        }
    }

    //根 左 右
    public void preOrder(){
        if (root == null){
            //TODO
        }else {
            preOrder(root);
        }
    }

    private void preOrder(Node root) {
        System.out.println(root.e);
        if (root.left != null){
            preOrder(root.left);
        }
        if (root.right != null){
            preOrder(root.right);
        }
    }

    /**中序遍历 顺序：左，根，右 **/
    public void inOrder(Node root) {
        if (root == null){
            return;
        }
        if (root.left != null){
            inOrder(root.left);
        }
        System.out.println(root.left);
        if (root.right != null){
            inOrder(root.right);
        }
    }


    /**
     *    后序遍历 顺序：左，右，根
     */
    public void postOrder(Node root){
        if (root == null){
            return;
        }
        if (root.left != null){
            postOrder(root.left);
        }
        if (root.right != null){
            postOrder(root.right);
        }
        System.out.println(root.e);
    }

    //是否包含某个节点
    public boolean contains(E e){
        return contains(root,e);
    }

    private boolean contains(Node root, E e) {
        if (root == null){
            return false;
        }
        if (e.compareTo(root.e) == 0){
            return true;
        }else if (e.compareTo(root.e) < 0){
            return contains(root.left,e);
        }else {
            return contains(root.right,e);
        }
    }

    public Node minimum(){
        return minimum(root);
    }

    private Node minimum(Node root) {
        if (root.left == null){
            return root;
        }
        return minimum(root.left);
    }
    public Node maximum(){
        return maximum(root);
    }

    private Node maximum(Node root) {
        if (root.right != null) {
            return root;
        }
        return maximum(root.right);
    }


    public Node removeMini(){
        root = removeMini(root);
        return root;
    }

    private Node removeMini(Node root) {
        if (root.left == null){
            Node cur = root.right;
            root.right = null;
            size--;
            return cur;
        }
        root.left = removeMini(root.left);
        return root;
    }

    public Node removeMaxi(){
        root = removeMaxi(root);
        return root;
    }

    private Node removeMaxi(Node root) {
        if (root.right == null){
            Node cur = root.left;
            root.left = null;
            size--;
            return cur;
        }
        root.right = removeMaxi(root.right);
        return root;
    }

    public void remove(E e){
        root = remove(root, e);
    }

    public Node remove(Node root,E e){
        if (root == null){
            return null;
        }

        if (e.compareTo(root.e) > 0){
             root.right = remove(root.right,e);
             return root;
        }else if (e.compareTo(root.e) < 0){
            root.left = remove(root.left,e);
            return root;
        }else { //e.compareTo(root.e) = 0
            if (root.left == null){
                Node delNode = root.right;
                root.right = null;
                size--;
                return delNode;
            }
            if (root.right == null){
                Node delNode = root.left;
                root.left = null;
                size--;
                return delNode;
            }

            //待删除节点左右子树均不为空的清空
            //找到比待删除节点大的最小节点，即待删除节点右子树的最小节点
            //用这个节点顶替待删除节点的位置
            Node successor = minimum(root.right);
            successor.right = removeMini(root.right);
            successor.left = root.left;
            root.left = root.right = null;
            return successor;
        }

    }

    public boolean isEmpty(){
        return size == 0;
    }


    //遍历二分搜索树
    @Override
    public String toString(){
        StringBuilder res = new StringBuilder();
        //前序遍历
//        preOrder(root);
        //中序遍历
        inOrder(root);
        //后序遍历
//        postOrder(root);
        return res.toString();
    }

    public static void main(String[] args) {
        BST3<Integer> bst3 = new BST3<>();
        bst3.add(5);
        bst3.add(3);
        bst3.add(7);
        bst3.add(1);
        bst3.add(4);
        bst3.add(2);
        bst3.add(6);
        System.out.println();
        System.out.println(bst3.contains(4));
        System.out.println(bst3.contains(10));
        System.out.println(bst3.minimum().e);
        System.out.println();
        bst3.remove(5);
        System.out.println(bst3);
    }
}

